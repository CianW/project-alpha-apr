from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task


def create_task(request):
    if not request.user.is_authenticated:
        return redirect("login")
    else:
        if request.method == "POST":
            form = TaskForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("list_projects")
        else:
            form = TaskForm()
    context = {
        "form": form,
    }

    return render(request, "createtask.html", context)


def show_my_tasks(request):
    if not request.user.is_authenticated:
        return redirect("login")
    else:
        my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_task_list": my_tasks,
    }
    return render(request, "mytasks.html", context)
