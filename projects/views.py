from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import ProjectForm


def list_projects(request):
    if not request.user.is_authenticated:
        return redirect("signup")
    else:
        projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/main.html", context)


def show_project(request, id):
    if not request.user.is_authenticated:
        return redirect("login")
    else:
        project = get_object_or_404(Project, id=id)
    context = {
        "project_object": project,
    }
    return render(request, "projects/detail.html", context)


def create_project(request):
    if not request.user.is_authenticated:
        return redirect("login")
    else:
        if request.method == "POST":
            form = ProjectForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("list_projects")
        else:
            form = ProjectForm()
    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
